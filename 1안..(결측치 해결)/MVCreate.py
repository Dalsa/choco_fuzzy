import pandas as pd
import numpy as np

#결측치를 만드는 프로그램
class MVcreate:

    def __init__(self,dataframe):
        self.dataframe = dataframe
        self.missing_value_list = [] #결측값 리스트
        self.predict_missing_value_list = [] #예측한 결측값 리스트

    def create_missing_value(self):
        i = 0
        for remove_data in self.dataframe:
            i = i + 1
            if(i == 7):
                remove_data = np.NAN
                i = 0
                self.missing_value_list.append(remove_data)

        print(self.dataframe.head(10))

