import pandas as pd
import glob
import os

def data_change_and_combine():
    csv_files = glob.glob('cctv_data/*.csv')
    all_data_frames = []

    for csv in csv_files:
        name = csv.split('.')[0] + '.csv'
        data_frame = pd.read_csv(csv, index_col=None, encoding='cp949', header=0)
        all_data_frames.append(data_frame)

    all_concat_data_frames = pd.concat(all_data_frames, axis=0, ignore_index=True)
    all_concat_data_frames.to_csv('busan_cctv.csv', index=None, encoding='cp949')

if __name__ == "__main__":
    data_change_and_combine()

