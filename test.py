import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(
    iris['data'], iris['target'], random_state=0
)
iris_dataframe = pd.DataFrame(X_train, columns=iris.feature_names)
print("갯수 : {}".format(iris_dataframe.iloc[:,0].count()))

data = np.empty((iris_dataframe.iloc[:,0].count(), 4), float)

data_array = iris_dataframe.iloc[:,:].values
print(data_array)