import pandas as pd
import numpy as np
import math

class fcm:
    DATA = 0 #데이터 갯수
    CLUSTER = 3 #클러스터링 분류 갯수 c(2 <= c < n)
    COORD = 4
    w_para = 2 #지수의 가중 m(1 < m < 무한대)
    Error = 0.01 #임계값

    iters = 0

    num = 0.0; den = 0.0; t_num = 0.0; t_den = 0.0
    temp1_dist = 0.0; temp2_dist = 0.0
    max_error = 0.0; sum = 0.0; NewValue = 0.0; count = 0
    sing = 0; cn = 0

    def __init__(self, X = pd.DataFrame()):
        self.X = X #데이터프레임 형식으로 넣는다고 가정함

        #Data 갯수 정하기
        self.DATA = self.X.iloc[:,0].count()

        #필요 인스턴스 변수 생성
        self.U = np.empty((self.CLUSTER, self.DATA), float)  # 새로운 소속 함수
        self.U_old = np.empty((self.CLUSTER, self.DATA), float)
        self.v = np.empty((self.CLUSTER, self.COORD), float)  # 퍼지 클러스터의 중심값
        self.d = np.empty((self.CLUSTER, self.DATA), float)
        self.x = np.empty((self.DATA, self.COORD), float)

        self.Rand_U = np.empty((self.DATA, self.CLUSTER), float)
        self.Init_Sum = np.empty((0, self.DATA), float)
        self.Temp_U = np.empty((self.DATA, self.CLUSTER), float)
        self.temp_e = np.empty((self.CLUSTER, self.DATA), float)

    def fit(self):
        self.x = self.X.iloc[:,:].values #ndarray로 변환
            
            
        self.u_matrix()

        while True:
            self.cal_vector()
            self.cal_center()
            self.cal_update()
            self.cal_error()
            if not self.count != 0: break

    def u_matrix(self): #Initialize Make U-matrix
        h = 0
        for i in range(0, self.CLUSTER):
            h += 1
            for j in range(0, self.DATA):
                self.U[i, j] = 0
                self.U[i, h] = 1

        print("소속 초기행렬")

        for i in range(0, self.CLUSTER):
            for j in range(0, self.DATA):
                print(self.U[i, j])
        print("\n")

        print("소속 초기 행렬 끝")
        self.iters = 0

    def cal_vector(self): #Calculate the center v
        print("cal_vector 실행")
        for i in range(0, self.CLUSTER):
            for j in range(0, self.COORD):
                self.num = 0
                self.den = 0
                for k in range(0, self.DATA):
                    self.t_num = 0
                    self.t_den = 0
                    self.t_num = pow(self.U[i, k], self.w_para) * self.x[k, j]
                    self.t_den = pow(self.U[i, k], self.w_para) #지수 함수
                    print("t_num = {0} t_den = {1}".format(self.t_num, self.t_den))
                    self.num += self.t_num
                    self.den += self.t_den
                self.v[i, j] = self.num / self.den
                print("\n")
            print("\n")
        print("\n")

        for i in range(0, self.CLUSTER):
            for j in range(0, self.COORD):
                print("{0} ".format(self.v[i, j]))
            print("\n")
        print("\n")

        print("cal_vector 끝")

    def cal_center(self): #Calculate distance between data and cluster center
        print("cal_center 실행")
        for i in range(0, self.CLUSTER):
            for j in range(0, self.DATA):
                self.temp2_dist = 0
                for k in range(0, self.COORD):
                    self.temp1_dist = pow((self.x[j, k] - self.v[i, k]), 2)
                    self.temp2_dist += self.temp1_dist
                self.d[i, j] = math.sqrt(self.temp2_dist)

        for i in range(0, self.CLUSTER):
            for j in range(0, self.DATA):
                print("d = {} \n".format(self.d[i, j]))

        print("cal_center 종료")

    def cal_update(self):
        for k in range(0, self.DATA):
            for i in range(0, self.CLUSTER):
                if self.d[i, k] != 0:
                    self.sum = 0
                    for j in range(0, self.CLUSTER):
                        if i == j:
                            self.sum += 1.0
                        elif self.d[j, k] == 0.0:
                            self.U[i, k] = 0
                            break
                        else:
                            self.sum += pow(self.d[i, k] / self.d[j, k], 2 / (self.w_para - 1))

                    self.NewValue = 1.0 / self.sum
                    self.U[i, k] = self.NewValue
                    print("U[{0}][{1}] = {2}".format(i, k, self.NewValue))
                else:
                    self.sing = 1
                    for j in range(0, i):
                        self.U[j, k] = 0.0

                    self.sing = 1
                    for j in range(i + 1, self.COORD):
                        if self.d[j, k] == 0:
                            self.sing = self.sing + 1

                    self.U[i, k] = 1.0 / self.sing

                    for j in range(i + 1, self.CLUSTER):
                        if self.d[j, k] == 0:
                            self.U[i, k] = 1.0 / self.sing
                        else:
                            self.U[i, k] = 0.0
                    break
            break

    def cal_error(self):
        self.max_error = 0.0

        for i in range(0, self.CLUSTER):
            for k in range(0, self.DATA):
                self.temp_e[i, k] = abs(self.U[i, k] - self.U_old[i, k])
                self.max_error = max(self.max_error, self.temp_e[i, k])

        for i in range(0, self.CLUSTER):
            for k in range(0, self.DATA):
                self.U_old[i, k] = self.U[i, k]

        print("error = {0}".format(self.max_error))
        self.count = 0

        if self.max_error > self.Error:
            self.count += 1

        self.iters += 1

    def print_cluster(self):
        print("---final cluster center---")

        for i in range(0, self.CLUSTER):
            for j in range(0, self.COORD):
                print("v[{0}][{1}] = {2}".format(i, j, self.v[i,j]))


        print("interation = {0} error = {1}".format(self.iters, self.max_error))

# if __name__ == "__main__":
#     from sklearn.datasets import load_iris
#     from sklearn.model_selection import train_test_split
# 
#     iris = load_iris()
#     X_train, X_test, y_train, y_test = train_test_split(
#         iris['data'], iris['target'], random_state=0
#     )
#     iris_dataframe = pd.DataFrame(X_train, columns=iris.feature_names)
# 
#     fcm = fcm(X=iris_dataframe)
#     fcm.fit()
#     fcm.print_cluster()




